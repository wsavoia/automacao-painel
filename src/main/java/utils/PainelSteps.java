package utils;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import automacaoTeste.AdministrarEspaco;
import automacaoTeste.Clientes;
import automacaoTeste.Divulgacao;
import automacaoTeste.Login;
import automacaoTeste.Logout;
import automacaoTeste.MeusDados;

public class PainelSteps {	
	
	WebDriver driver = OpenChrome.inicializar();
	
	@Test
	public void fluxo() throws Exception {
		
		
		Login logar = new Login(driver);	
		logar.LoginPainel();
		
		Thread.sleep(2000);
		
		Clientes acessarClientes = new Clientes(driver);
		acessarClientes.AcessarClientes();
		
		Divulgacao dispararDisparador = new Divulgacao(driver);
		dispararDisparador.DispararEmail();
		dispararDisparador.EscolherLista();
		
		MeusDados meusDados = new MeusDados(driver);
		meusDados.AcessarMeusDados();
		
		AdministrarEspaco administrarEspaco = new AdministrarEspaco(driver);
		administrarEspaco.AcessarAdministrarEspaco();
		
		Logout logout = new Logout(driver);
		logout.LogoutPainel();
		
	}
}