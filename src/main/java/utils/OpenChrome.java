package utils;

import org.openqa.selenium.chrome.ChromeDriver;

public class OpenChrome {
	
	private static ChromeDriver driver;

	public static ChromeDriver inicializar(){

		System.setProperty("webdriver.chrome.driver", "C://Users//wagnerj//Documents//Workspace(Selenium)//AutomacaoTeste/Drivers//chromedriver//chromedriver.exe");
		driver = new ChromeDriver();	
		driver.manage().window().maximize();
		
		return driver;
	}
}