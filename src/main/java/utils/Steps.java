package utils;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class Steps {
	
	@Dado("^que eu escolha o navegador$")
	public void que_eu_escolha_o_navegador() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^eu escolho com o valor \"([^\"]*)\"$")
	public void eu_escolho_com_o_valor(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Então("^eu devo ver o resultado \"([^\"]*)\"$")
	public void eu_devo_ver_o_resultado(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Dado("^que na tela de login, os campos email e senha estejam visíveis$")
	public void que_na_tela_de_login_os_campos_email_e_senha_estejam_visíveis() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^eu preencho o login com o valor \"([^\"]*)\"$")
	public void eu_preencho_o_login_com_o_valor(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^preencho a senha com o valor \"([^\"]*)\"$")
	public void preencho_a_senha_com_o_valor(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^pressiono o botao OK$")
	public void pressiono_o_botao_OK() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Então("^eu sou direcionado para a home do Painel CND$")
	public void eu_sou_direcionado_para_a_home_do_Painel_CND() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

}