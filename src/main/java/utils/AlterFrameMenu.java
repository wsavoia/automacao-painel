package utils;

import org.openqa.selenium.WebDriver;

public class AlterFrameMenu {
	
	private WebDriver driver = null;
	
	public AlterFrameMenu(WebDriver driver){
		this.driver = driver;
	}
	
	public void frameMenuPainel() throws InterruptedException{

		Thread.sleep(1000);
		
		driver.switchTo().frame("menu_painel_cnd_bra");
		
	}

}
