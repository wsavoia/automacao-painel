package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TakeDateTime {
	
	public static String getDateTime() {
	
	LocalDateTime date = LocalDateTime.now();
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy - HH-mm-ss");
	String localDateTime = date.format(formatter);
	
	return localDateTime;
	
	}

}
