package automacaoTeste;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.TakeScreenshot;

import static org.junit.Assert.assertTrue;

public class Login {
	
	private WebDriver driver = null;
	
	public Login(WebDriver driver){
		this.driver = driver;
	}
	
	public void LoginPainel() throws InterruptedException{
	
	driver.get("http://painel.rede.natura.net");
	
	Thread.sleep(2000);
	
	WebElement usuario = driver.findElement(By.name("uEmail"));
	WebElement senha = driver.findElement(By.name("uPassword"));
	WebElement botaoLogar = driver.findElement(By.id("okButton"));
	
	usuario.sendKeys("citteste1@gmail.com");
	senha.sendKeys("a1a2a3");
	botaoLogar.submit();
	
	Thread.sleep(3000);
	
	boolean loginValidator = driver.getPageSource().contains("MEU PAINEL");
	assertTrue(loginValidator);
	
    TakeScreenshot.getScreenshot(driver, "LoginSucessHome"); 
	
	}
}