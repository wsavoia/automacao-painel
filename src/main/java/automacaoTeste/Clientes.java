package automacaoTeste;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.AlterFrameMenu;

import java.util.Scanner;

public class Clientes {
	
	private WebDriver driver = null;
	private Scanner entrada;
	
	public Clientes(WebDriver driver){
		this.driver = driver;
	}
	
	public void AcessarClientes() throws Exception {
		
	AlterFrameMenu frameMenu = new AlterFrameMenu(driver);
	frameMenu.frameMenuPainel();
		
	driver.findElement(By.xpath("//*[@id=\"null-link-visitas-clientes\"]")).click();
	Thread.sleep(500);
	driver.findElement(By.id("menu-link-clientes")).click();
	
	escolhaDivulgar();
		
	}
	
	public void escolhaDivulgar() throws Exception {
		
	entrada = new Scanner(System.in);
	System.out.println("Deseja divulgar para seus clientes? (S/N)");
	String escolha = entrada.nextLine();	
	
	driver.switchTo().defaultContent();
	
		if (escolha .equals("S")) {
			WebElement divulgar = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[3]/a[2]"));
			divulgar.click();
			
			Divulgacao dispararDisparador = new Divulgacao(driver);
			dispararDisparador.divulgar(driver);
			
		} else if (escolha .equals("N")) {
				
		} else {
			System.out.println("Resposa inv�lida");
			escolhaDivulgar();
		}
	}
}