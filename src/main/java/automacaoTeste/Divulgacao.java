package automacaoTeste;

import java.util.Scanner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.AlterFrameMenu;

public class Divulgacao {
	
	private WebDriver driver = null;
	private Scanner entrada;
	
	public Divulgacao(WebDriver driver){
		this.driver = driver;
	}
	
	public void DispararEmail() throws Exception {
		
		AlterFrameMenu frameMenu = new AlterFrameMenu(driver);
		frameMenu.frameMenuPainel();
		
		driver.findElement(By.xpath("//*[@id=\"null-link-divulgacao\"]")).click();
		driver.findElement(By.id("menu-link-listas")).click();
		
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		
	}
		
	public void EscolherLista() throws InterruptedException {
		
		entrada = new Scanner(System.in);
		System.out.println("Informe para qual lista deseja enviar:");
		System.out.println("1 - Amigos / 2 - Trabalho / 3 - Familia / 4 - Outros  / 5 - Todos / 6 - N�o Enviar");
		int lista = entrada.nextInt();	
	
			switch (lista) {
			case 1:
				divulgarParaListagem();
				EscolherLista();
				break;
			case 2:
				AbaTrabalho();
				EscolherLista();
			break;
			case 3:
				AbaFamilia();
				EscolherLista();
			break;
			case 4:
				AbaOutros();
				EscolherLista();
			break;
			case 5:
				divulgarParaListagem();
				Thread.sleep(2000);
				AbaTrabalho();
				Thread.sleep(2000);
				AbaFamilia();
				Thread.sleep(2000);
				AbaOutros();
			break;
			case 6:
			break;
			default: 
				System.out.println("Resposta inv�lida!");
				EscolherLista();
			break;
			}
		}
	
		public void AbaTrabalho() throws InterruptedException{
			WebElement mudarAbaTrabalho = driver.findElement(By.xpath("//*[@id=\"page-wrapper\"]/div[2]/div/div[2]/ul/li[2]/a"));
			mudarAbaTrabalho.click();
			divulgarParaListagem();
		}
		
		public void AbaFamilia() throws InterruptedException{
			WebElement mudarAbaFamilia = driver.findElement(By.xpath("//*[@id=\"page-wrapper\"]/div[2]/div/div[2]/ul/li[3]/a"));
			mudarAbaFamilia.click();
			divulgarParaListagem();
		}
		
		public void AbaOutros() throws InterruptedException{
			WebElement mudarAbaOutros = driver.findElement(By.xpath("//*[@id=\"page-wrapper\"]/div[2]/div/div[2]/ul/li[4]/a"));
			mudarAbaOutros.click();
			divulgarParaListagem();
		}
		
		public void divulgarParaListagem() throws InterruptedException {
				
			WebElement divulgarParaListagem = driver.findElement(By.xpath("//*[@id=\"page-wrapper\"]/div[2]/div/div[2]/div/div/div[2]/div[1]/a[2]"));
			divulgarParaListagem.click();
			
			Thread.sleep(1000);
			
			divulgar(driver);
		}
			
		public void divulgar(WebDriver driver) throws InterruptedException {
			
			for (int i = 1; i < 4; i++) {
				WebElement alterarTemplate = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[2]/div/div/rn-mail-trigger-modal/div/div/div/div[2]/div[2]/div[1]/div[1]/div/a[2]/span[1]"));
				Thread.sleep(500);
				alterarTemplate.click();
			}
		
			WebElement tituloEmail = driver.findElement(By.name("mail-trigger-subject"));
			tituloEmail.sendKeys("Natura Cosm�ticos - Cupom de 5% de desconto (ABRIL)");
		
			WebElement corpoEmail = driver.findElement(By.name("mail-trigger-body"));
			corpoEmail.sendKeys("Ol�!<br><br>Cupom de 5% de desconto em qualquer pedido!<br><br>A Natura est� com diversas promo��es<br>Disponibilizamos um cupom de 5% em qualquer pedido!<br><br>Fa�a seu pedido e receba no conforto da sua casa!<br>FRETE GR�TIS ACIMA DE R$ 119,90<br><br>Acesse agora mesmo e confira: <a href=\"www.natura.com.br/consultoria/lcosmeticos\">https://rede.natura.net/espaco/lcosmeticos</a><br><br>Cupom: ABRIL<br><br>Qualquer d�vida, pode entrar em contato conosco!<br><br>Tenha um �timo ABRIL e boas compras!");
		
			WebElement enviarEmail = driver.findElement(By.xpath("//*[@id=\"mail-trigger-modal\"]/div/div/div[3]/button[2]"));
			enviarEmail.click();
		
			WebElement confirmarEnvio = driver.findElement(By.xpath("//*[@id=\"mail-trigger-modal\"]/div/div/div[4]/button[2]"));
			confirmarEnvio.click();
			
			Thread.sleep(2000);
		
			WebElement fecharConfirmacao = driver.findElement(By.xpath("//*[@id=\"mail-trigger-modal\"]/div/div/div[1]/button"));
			fecharConfirmacao.click();
		}
}
