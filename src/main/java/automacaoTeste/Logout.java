package automacaoTeste;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.TakeScreenshot;

public class Logout {

	private WebDriver driver = null;

	public Logout(WebDriver driver){
		this.driver = driver;
	}
	
	public void LogoutPainel() throws InterruptedException{
		Thread.sleep(2000);
		
		WebElement logout = driver.findElement(By.className("logout"));
		logout.click();
		
		Thread.sleep(1000);
		
		boolean logoutValidator = driver.getPageSource().contains("BEM-VINDO(A) AO SEU PAINEL DE NEGÓCIOS.");
		assertTrue(logoutValidator);
		
        TakeScreenshot.getScreenshot(driver, "LogoutSuccess"); 
	}
}