package automacaoTeste;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utils.TakeScreenshot;

public class AdministrarEspaco {
	
	private WebDriver driver = null;
	
	public AdministrarEspaco(WebDriver driver){
		this.driver = driver;
	}
	
	public void AcessarAdministrarEspaco() throws InterruptedException{
	Thread.sleep(3000);
	
    TakeScreenshot.getScreenshot(driver, "AdminEspaco");
		
	WebElement administrarEspaco = driver.findElement(By.xpath("//*[@id=\"page-wrapper\"]/div[2]/div/div[3]/div/div[2]/rn-personal-data-functions/div/div[2]/div[2]/span/a"));
	administrarEspaco.click();
	
	Thread.sleep(2000);
	WebElement atualizarCartao = driver.findElement(By.xpath("//*[@id=\"page-wrapper\"]/div[2]/div/div[1]/div/div[4]/div[2]/div/rn-credit-card-form/div/span[1]/a/span"));
	atualizarCartao.click();

	Thread.sleep(1000);
	WebElement numCartao = driver.findElement(By.name("cardNumber"));
	numCartao.sendKeys("5555666677778884");
	numCartao.sendKeys(Keys.TAB);
	
	WebElement nomeCartao = driver.findElement(By.name("cardName"));
	nomeCartao.sendKeys("TESTE AUTOMACAO");
	numCartao.sendKeys(Keys.TAB);
	
	//WebElement mesCartao = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div[4]/div[2]/div/rn-credit-card-form/div/div/div[1]/form/div[1]/div[4]/select"));
	//((Select) mesCartao).selectByValue("03");
	
	//WebElement anoCartao = driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div/div[1]/div/div[4]/div[2]/div/rn-credit-card-form/div/div/div[1]/form/div[1]/div[5]/select"));
	//((Select) anoCartao).selectByVisibleText("2021");
	
	WebElement salvarCartao = driver.findElement(By.xpath("//*[@id=\"RNUpdateCreditcard\"]/div[2]/div/button"));
	salvarCartao.click();	
	
    TakeScreenshot.getScreenshot(driver, "SalvarCartao"); 
	}
}