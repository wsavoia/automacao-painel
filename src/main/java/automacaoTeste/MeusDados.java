package automacaoTeste;

import static org.junit.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.TakeScreenshot;


public class MeusDados {
	
	private WebDriver driver = null;
	
	public MeusDados(WebDriver driver){
		this.driver = driver;
	}
	
	public void AcessarMeusDados() throws Exception{
		
		WebElement configuracoes = driver.findElement(By.xpath("//*[@id=\"side-menu\"]/li/div[1]/a/span/span[2]/span"));
		Thread.sleep(1000);
		configuracoes.click();
				
		WebElement meusdados = driver.findElement(By.xpath("//*[@id=\"side-menu\"]/li/div[1]/ul/li[1]/a/span"));
		Thread.sleep(1000);
		meusdados.click();
		
		Thread.sleep(2000);
		
		WebElement cep = driver.findElement(By.name("cep"));
		cep.clear();
		cep.sendKeys("13056487");
		cep.sendKeys(Keys.TAB);
		
		Thread.sleep(1000);
		
		WebElement numero = driver.findElement(By.name("streetNumber"));
		numero.clear();
		numero.sendKeys("999");
		
		WebElement salvar = driver.findElement(By.xpath("//*[@id=\"RNPersonalDataForm\"]/div[17]/button[2]/span"));
		salvar.click();		
		
        TakeScreenshot.getScreenshot(driver, "SalvarEndereco"); 
        
		Thread.sleep(1000);
		
		boolean saveValidator = driver.getPageSource().contains("Dados atualizados com sucesso");
		assertTrue(saveValidator);
		
	    TakeScreenshot.getScreenshot(driver, "SaveAddressSuccess");
		
		WebElement fecharSalvar = driver.findElement(By.xpath("//*[@id=\"modal-mensagens\"]/div[2]/div/div[1]/button"));
		fecharSalvar.click();		
	}

}
